<?php
require __DIR__ . '/../vendor/autoload.php';


use Crunch\StateMachine\State;
use Crunch\StateMachine\CallbackTransition;
use Crunch\StateMachine\Machine;
use Crunch\StateMachine\ActionEvent;
use Crunch\StateMachine\Event;
use Crunch\StateMachine\StaticDefinition;

$states = array(
    new State('uncolored'),
    new State('green'),
    new State('yellow')
);
$transitions = array(
    new CallbackTransition('uncolored', 'green', function (ActionEvent $event) { return $event->action == 'make green'; }),
    new CallbackTransition('green', 'yellow', function (ActionEvent $event) { return $event->action == 'make yellow'; })
);
$listener = new \Crunch\StateMachine\CallbackListener(function($hookName, Machine $machine, Event $event) {
    var_dump($hookName . ': ' . ($machine->currentState ? $machine->currentState->id : ''));
});

$machine = new \Crunch\StateMachine\Machine(new StaticDefinition($states, $transitions), $listener);

$machine->start('uncolored');

$machine->push(new ActionEvent('make green'));
$machine->push(new ActionEvent('make yellow'));

try {
    $machine->push(new ActionEvent('make foo'));
} catch (\Exception $e) {
    var_dump($e->getMessage());
}

echo "Machine before paused: {$machine->currentState->id}\n";
$pause = $machine->pause();
echo "Pause data '$pause'\n";
echo "Machine after paused: {$machine->currentState}\n";
$machine->resume($pause);
echo "Machine after resume: {$machine->currentState->id}\n";
