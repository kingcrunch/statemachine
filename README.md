Crunch\StateMachine [![Build Status](https://secure.travis-ci.org/KingCrunch/StateMachine.png)](http://travis-ci.org/KingCrunch/StateMachine)
============
Create customizable state machines.

State machines are useful in any case, where a state of an entity must be tracked. A popular
example is an order-and-payment-component, where the engine needs to keep track of the order,
payment and shipping states.

* [List of available packages at packagist.org](http://packagist.org/packages/crunch/state-machine)
  (See also [Composer: Declaring dependencies](http://getcomposer.org/doc/00-intro.md#declaring-dependencies))

Requirements
============
* PHP => 5.3

Contributors
============
See CONTRIBUTING.md for details on how to contribute.

* Sebastian "KingCrunch" Krebs <krebs.seb@gmail.com> -- http://www.kingcrunch.de/

License
=======
This library is licensed under the MIT License. See the LICENSE file for details.
