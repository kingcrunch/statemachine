<?php
namespace Crunch\StateMachine;

/**
 * Event
 *
 * In fact an event is completely arbitrary, because it's completely up to the corresponding
 * transitions to accept it, or not. In most cases the ActionEvent is fine.
 */
interface Event {
}
