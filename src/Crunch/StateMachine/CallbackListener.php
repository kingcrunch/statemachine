<?php
namespace Crunch\StateMachine;

/**
 * CallbackListener
 *
 * Listener, that calls
 */
class CallbackListener implements Listener {
    const BEFORE_EVENT = 'on_before_event';
    const LEAVE_STATE = 'on_leave_state';
    const TRANSITION = 'on_transition';
    const ENTER_STATE = 'on_enter_state';
    const AFTER_EVENT = 'on_after_event';

    /**
     * The listener callback
     *
     * @var callable
     */
    protected $callback;

    /**
     * Creates new callback listener
     *
     * Accepts a callback function, that depending on the called hook receives 3, or 4 arguments
     *
     * - The called hook as string, see CallbackListener-constants
     * - The calling machine
     * - The event
     * - Either the state (origin, or target), or the transition, or none.
     *
     * @param callable $callback
     */
    public function __construct($callback) {
        $this->callback = $callback;
    }

    /**
     * @param Machine $machine
     * @param Event   $event
     */
    public function onBeforeEvent (Machine $machine, Event $event) {
        $callback = $this->callback;
        $callback(self::BEFORE_EVENT, $machine, $event);
    }

    /**
     * @param Machine $machine
     * @param Event   $event
     * @param State   $state
     */
    public function onLeaveState (Machine $machine, Event $event, State $state) {
        $callback = $this->callback;
        $callback(self::LEAVE_STATE, $machine, $event);
    }

    /**
     * @param Machine    $machine
     * @param Event      $event
     * @param Transition $transition
     */
    public function onTransition (Machine $machine, Event $event, Transition $transition) {
        $callback = $this->callback;
        $callback(self::TRANSITION, $machine, $event, $transition);
    }

    /**
     * @param Machine $machine
     * @param Event   $event
     * @param State   $state
     */
    public function onEnterState (Machine $machine, Event $event, State $state) {
        $callback = $this->callback;
        $callback(self::ENTER_STATE, $machine, $event, $state);
    }

    /**
     * @param Machine $machine
     * @param Event   $event
     */
    public function onAfterEvent (Machine $machine, Event $event) {
        $callback = $this->callback;
        $callback(self::AFTER_EVENT, $machine, $event);
    }
}
