<?php
namespace Crunch\StateMachine;

class State {
    public $id;
    public $start = true;
    public $end = true;

    public function __construct ($id, $start = null, $end = null) {
        $this->id = $id;
        is_null($start) or $this->start = $start;
        is_null($end) or $this->end = $end;
    }

    public function pause () { return null; }
    public function resume ($resume) {}
    public function reset () {}
}
