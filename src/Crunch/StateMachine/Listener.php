<?php
namespace Crunch\StateMachine;

/**
 * Listener interface
 *
 * The machine triggers the listener every time a specific event (not an event
 * represented by the Event-interface) occurs. For now the return values were
 * not used, but this may change, thus to stay safe and always relying on the
 * default behaviour, ensure, that it returns "null", or nothing at all ("void"),
 * than everything will remain, as it is.
 */
interface Listener {
    /**
     * @param Machine $machine
     * @param Event   $event
     * @return void
     */
    public function onBeforeEvent (Machine $machine, Event $event);

    /**
     * @param Machine $machine
     * @param Event   $event
     * @param State   $state
     * @return void
     */
    public function onLeaveState (Machine $machine, Event $event, State $state);

    /**
     * @param Machine    $machine
     * @param Event      $event
     * @param Transition $transition
     * @return void
     */
    public function onTransition (Machine $machine, Event $event, Transition $transition);

    /**
     * @param Machine $machine
     * @param Event   $event
     * @param State   $state
     * @return void
     */
    public function onEnterState (Machine $machine, Event $event, State $state);

    /**
     * @param Machine $machine
     * @param Event   $event
     * @return void
     */
    public function onAfterEvent (Machine $machine, Event $event);
}
