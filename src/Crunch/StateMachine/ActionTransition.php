<?php
namespace Crunch\StateMachine;

/**
 * Transition, that only takes the $action from an ActionEvent into account
 */
class ActionTransition extends Transition {
    /**
     * @var string
     */
    protected $action;

    /**
     * @param string $origin
     * @param string $target
     * @param string $action
     */
    public function __construct ($origin, $target, $action) {
        parent::__construct($origin, $target);
        $this->action = $action;
    }

    /**
     * Whether or not this transition accepts the new event
     *
     * @param Event $event
     * @return boolean
     */
    public function accept (Event $event) {
        return $event instanceof ActionEvent && $event->action == $this->action;
    }
}
