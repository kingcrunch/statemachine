<?php
namespace Crunch\StateMachine;

/**
 * Callback transition
 *
 * A simple transition, that relies on a callback to decide, whether or
 * not it accepts an event.
 */
class CallbackTransition extends Transition {
    /**
     * @var callable
     */
    protected $callback;

    /**
     * @param string $origin
     * @param string $target
     * @param callable $accept
     */
    public function __construct ($origin, $target, $accept) {
        $this->callback = $accept;
        parent::__construct($origin, $target);
    }

    /**
     * @param Event $event
     * @return boolean
     */
    public function accept (Event $event) {
        $callback = $this->callback;
        return $callback($event);
    }
}
