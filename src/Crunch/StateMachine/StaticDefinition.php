<?php
namespace Crunch\StateMachine;

class StaticDefinition implements Definition {
    protected $states = array();
    protected $transitions = array();

    public function __construct (array $states, array $transitions) {
        foreach ($states as $state) $this->addState($state);
        foreach ($transitions as $transition) $this->addTransition($transition);
    }

    public function addState (State $state) {
        if (isset($this->states[$state->id])) throw new \Exception("State '{$state->id}' already registered'");

        $this->states[$state->id] = $state;
        $this->transitions[$state->id] = array();
    }

    /**
     * @param $stateId
     * @return State
     * @throws \Exception
     */
    public function retrieveState ($stateId) {
        if (!isset($this->states[$stateId])) throw new \Exception("Unknown state '$stateId'");

        return $this->states[$stateId];
    }

    public function addTransition (Transition $transition) {
        if (!isset($this->states[$transition->origin])) throw new \Exception("Unknown origin state '{$transition->origin}'");
        if (!isset($this->states[$transition->target])) throw new \Exception("Unknown origin state '{$transition->target}'");

        $this->transitions[$transition->origin][] = $transition;
    }

    public function resolve (State $state, Event $event) {
        if (!isset($this->states[$state->id])) throw new \Exception("Unknown origin state '{$state->id}'");

        $candidates = array_filter(
            $this->transitions[$state->id],
            function (Transition $transition) use ($event) { return $transition->accept($event); }
        );

        if (count($candidates) == 0) throw new \Exception("No transition found from '{$state->id}'");
        if (count($candidates) > 1) throw new \Exception("Too many transitions found from '{$state->id}'");

        return $candidates[0];
    }

    public function pauseStates () {
        $resumeData = array_map(
            function (State $state) { return $state->pause(); },
            $this->states
        );
        return array_filter($resumeData, function ($data) { return !is_null($data); });
    }

    public function resumeStates (array $stateData) {
        foreach ($stateData as $stateId => $data) {
            $this->retrieveState($stateId)->resume($data);
        }
    }

    public function resetStates () {
        array_walk($this->states, function(State $state) { $state->reset(); });
    }
}
