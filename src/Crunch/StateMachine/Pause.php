<?php
namespace Crunch\StateMachine;

class Pause implements \Serializable {
    public $stateId;
    public $stateData;
    public function __construct ($stateId, array $stateData) {
        $this->stateId = $stateId;
        $this->stateData = $stateData;
    }
    public function __toString () {
        return $this->stateId . ($this->stateData? ':' . json_encode($this->stateData) : '');
    }
    public static function build ($string) {
        list($stateId, $stateData) = array_pad(explode(':', $string, 2), 2 , array());
        return new static ($stateId, json_decode($stateData));
    }

    public function serialize () {
        return serialize(array($this->stateId, $this->stateData));
    }

    public function unserialize ($serialized) {
        list($this->stateId, $this->stateData) = unserialize($serialized);
    }
}
