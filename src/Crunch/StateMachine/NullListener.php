<?php
namespace Crunch\StateMachine;

/**
 * A listener, that simply does nothing
 *
 * Remind the headline when you consider to extend this class: In this case it should
 * cover the new requirements with "do nothing" too.
 */
class NullListener implements Listener {
    /**
     * @param Machine $machine
     * @param Event   $event
     * @return void
     */
    public function onBeforeEvent (Machine $machine, Event $event) {}

    /**
     * @param Machine $machine
     * @param Event   $event
     * @param State   $state
     * @return void
     */
    public function onLeaveState (Machine $machine, Event $event, State $state) {}

    /**
     * @param Machine    $machine
     * @param Event      $event
     * @param Transition $transition
     * @return void
     */
    public function onTransition (Machine $machine, Event $event, Transition $transition) {}

    /**
     * @param Machine $machine
     * @param Event   $event
     * @param State   $state
     * @return void
     */
    public function onEnterState (Machine $machine, Event $event, State $state) {}

    /**
     * @param Machine $machine
     * @param Event   $event
     * @return void
     */
    public function onAfterEvent (Machine $machine, Event $event) {}
}
