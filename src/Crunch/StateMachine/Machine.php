<?php
namespace Crunch\StateMachine;

class Machine {
    /**
     * Current state
     *
     * In some cases this is `null`:
     * - In transition
     * - before start()
     *
     * @readonly
     * @var State|null
     */
    public $currentState = null;
    /**
     * @var Definition
     */
    protected $definition;

    /**
     * @readonly
     * @var Listener
     */
    protected $listener;

    /**
     * Array of unprocessed events
     *
     * @var Event[]
     */
    protected $events = array();

    /**
     * Wether or not this machine is currently processing events
     *
     * @readonly
     * @var bool
     */
    public $processing = false;

    /**
     * Creates new machine
     *
     * @param Definition         $definition
     * @param Listener        $listener
     * @throws \Exception
     */
    public function __construct (Definition $definition, Listener $listener) {
        $this->definition = $definition;

        $this->listener = $listener;
    }

    /**
     * Start machine at $state
     *
     * @param string $stateId
     * @throws \Exception
     */
    public function start ($stateId) {
        if ($this->currentState) throw new \Exception('Machine already running');
        $state = $this->definition->retrieveState($stateId);
        if (!$state->start) throw new \Exception("State '$stateId' is not a valid start state");

        $this->currentState = $state;
    }

    public function pause () {
        if (!$this->currentState) throw new \Exception('Machine not running');
        $pause = new Pause($this->currentState->id, $this->definition->pauseStates());
        $this->reset();
        return $pause;
    }

    public function resume (Pause $pause) {
        if ($this->currentState) throw new \Exception('Machine already running');
        $this->definition->resumeStates($pause->stateData);
        $this->currentState = $this->definition->retrieveState($pause->stateId);
    }

    public function reset () {
        if (!$this->currentState) throw new \Exception('Machine not running');
        $this->definition->resetStates();
        $this->currentState = null;
    }

    public function stop () {
        if (!$this->currentState) throw new \Exception('Machine not running');
        if (!$this->currentState->end) throw new \Exception("Current state '{$this->currentState->id}' is not a valid end state");
        $this->reset();
    }

    /**
     * Push new event
     *
     * @param Event $event
     * @throws \Exception
     */
    public function push (Event $event) {
        $this->events[] = $event;

        if (!$this->processing)  $this->process();
    }

    public function process () {
        $this->processing = true;

        while ($event = array_shift($this->events)) {
            if (!$this->currentState) throw new \Exception('Machine not running');

            $this->listener->onBeforeEvent($this, $event);

            $transition = $this->definition->resolve($this->currentState, $event);

            $this->listener->onLeaveState($this, $event, $this->currentState);
            $this->currentState = null;
            $this->listener->onTransition($this, $event, $transition);
            $this->currentState = $this->definition->retrieveState($transition->target);
            $this->listener->onEnterState($this, $event, $this->currentState);
            $this->listener->onAfterEvent($this, $event);

            if (!$this->currentState) throw new \Exception('Machine not left in a state');
        }

        $this->processing = false;
    }
}
