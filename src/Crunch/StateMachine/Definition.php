<?php
namespace Crunch\StateMachine;

interface Definition {
    public function retrieveState ($stateId);
    public function resolve (State $state, Event $event);
    public function pauseStates ();
    public function resumeStates (array $stateData);
    public function resetStates ();
}
