<?php
namespace Crunch\StateMachine;

abstract class Transition {
    /**
     * @var string
     */
    public $origin;
    /**
     * @var string
     */
    public $target;

    /**
     * @param string $origin
     * @param string $target
     */
    public function __construct ($origin, $target) {
        $this->origin = (string) $origin;
        $this->target = (string) $target;
    }

    /**
     * Whether or not this transition accepts the new event
     *
     * @param Event $event
     * @return boolean
     */
    abstract public function accept (Event $event);
}
