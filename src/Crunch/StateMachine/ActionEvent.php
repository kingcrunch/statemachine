<?php
namespace Crunch\StateMachine;

/**
 * Event, that carries a string representing an action
 */
class ActionEvent implements Event {
    /**
     * @readonly
     * @var string
     */
    public $action;

    /**
     * @param string $action
     */
    public function __construct ($action) {
        $this->action = $action;
    }
}
