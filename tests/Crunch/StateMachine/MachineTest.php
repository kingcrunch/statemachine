<?php
namespace Crunch\StateMachine;

use PHPUnit_Framework_TestCase as TestCase;

class MachineTest extends TestCase {
    public function testSimpleUsecase () {
        $machine = $this->createSimpleActionMachine();
        $machine->start('first');
        $this->assertEquals('first', $machine->currentState->id);
    }

    protected function createSimpleActionMachine () {
        parent::setUp();

        $states = array(
            new State('first'),
            new State('second'),
            new State('third')
        );

        $transitions = array(
            new ActionTransition('first', 'second', 'go to second'),
            new ActionTransition('second', 'third', 'got to third'),
            new ActionTransition('third', 'first', 'go to first'),
            new ActionTransition('first', 'third', 'go to third')
        );

        return new Machine(new StaticDefinition($states, $transitions), new NullListener);
    }
}
